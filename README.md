From http://osmstudios.com/tutorials/your-first-love2d-game-in-200-lines-part-1-of-3

# Credits:

## Images:

* Player aircraft from http://opengameart.org/content/aircrafts
* Enemy aircraft and bullets from tutorial assets zip (see tutorial URL above)

## Sounds:

* Bullet sounds from tutorial assets zip file (see tutorial URL above)
* Enemy explosion from http://opengameart.org/content/boom-pack-1
* Player explosion from http://opengameart.org/content/big-explosion
