debug = true

player = {x = 200, y = 710, speed = 200, image = nil}

canShoot = true
canShootTimerMax = 0.2
canShootTimer = canShootTimerMax

bulletImage = nil

bullets = {}

createEnemyTimerMax = 0.4
createEnemyTimer    = createEnemyTimerMax

enemyImage = nil

enemies = {}

isAlive = true
score   = 0


function love.load(arg)
    player.image    = love.graphics.newImage('assets/plane.png')
    bulletImage     = love.graphics.newImage('assets/bullet.png')
    enemyImage      = love.graphics.newImage('assets/enemy.png')
    gunSound        = love.audio.newSource('assets/gun-sound.wav', 'static')
    enemyBoomSound  = love.audio.newSource('assets/boom8.wav', 'static')
    playerBoomSound = love.audio.newSource('assets/DeathFlash.wav', 'static')
end

function love.update(dt)
    canShootTimer = canShootTimer - (1 * dt)
    if canShootTimer < 0 then
        canShoot = true
    end

    for i, bullet in ipairs(bullets) do
        bullet.y = bullet.y - (250 * dt)
        if bullet.y < 0 then
            table.remove(bullets, i)
        end
    end

    createEnemyTimer = createEnemyTimer - (1 * dt)
    if createEnemyTimer < 0 then
        createEnemyTimer = createEnemyTimerMax

        randomNumber = math.random(10, love.graphics.getWidth() - 10)
        newEnemy = {x = randomNumber, y = -10, image = enemyImage}
        table.insert(enemies, newEnemy)
    end

    for i, enemy in ipairs(enemies) do
        enemy.y = enemy.y + (200 * dt)

        if enemy.y > 850 then -- remove enemies when they pass off the screen
            table.remove(enemies, i)
        end
    end

    for i, enemy in ipairs(enemies) do
        for j, bullet in ipairs(bullets) do
            if CheckCollision(enemy.x, enemy.y, enemy.image:getWidth(), enemy.image:getHeight(), bullet.x, bullet.y, bullet.image:getWidth(), bullet.image:getHeight()) then
                table.remove(bullets, j)
                table.remove(enemies, i)
                enemyBoomSound:play()
                score = score + 1
            end
        end

        if CheckCollision(enemy.x, enemy.y, enemy.image:getWidth(), enemy.image:getHeight(), player.x, player.y, player.image:getWidth(), player.image:getHeight())
            and isAlive then
            table.remove(enemies, i)
            enemyBoomSound:play()
            playerBoomSound:play()
            isAlive = false
        end
    end

    if love.keyboard.isDown('escape') then
        love.event.push('quit')
    end

    if not isAlive and love.keyboard.isDown('r') then
        bullets = {}
        enemies = {}
        canShootTimer = canShootTimerMax
        createEnemyTimer = createEnemyTimerMax
        player.x = 50
        player.y = 710
        score = 0
        isAlive = true
    elseif love.keyboard.isDown(' ', 'rctrl', 'lctrl', 'ctrl') and canShoot then
        newBullet = {x = player.x + (player.image:getWidth()/2), y = player.y, image = bulletImage}
        table.insert(bullets, newBullet)
        gunSound:play()
        canShoot = false
        canShootTimer = canShootTimerMax
    elseif love.keyboard.isDown('left', 'a') then
        if player.x > 0 then
            player.x = player.x - (player.speed * dt)
        end
    elseif love.keyboard.isDown('right', 'd') then
        if player.x < (love.graphics.getWidth() - player.image:getWidth()) then
            player.x = player.x + (player.speed * dt)
        end
    elseif love.keyboard.isDown('up', 'w') then
        if player.y > 0 then
            player.y = player.y - (player.speed * dt)
        end
    elseif love.keyboard.isDown('down', 's') then
        if player.y < (love.graphics:getHeight() - player.image:getHeight()) then
            player.y = player.y + (player.speed * dt)
        end
    end

end

function love.draw(dt)
    for i, bullet in ipairs(bullets) do
        love.graphics.draw(bullet.image, bullet.x, bullet.y)
    end
    for i, enemy in ipairs(enemies) do
        love.graphics.draw(enemy.image, enemy.x, enemy.y)
    end
    if isAlive then
        love.graphics.draw(player.image, player.x, player.y)
    else
        love.graphics.print("Press 'R' to restart", love.graphics:getWidth()/2 - 50, love.graphics:getHeight()/2 - 10)
    end
    love.graphics.setColor(255, 255, 255)
    love.graphics.print("Score: " .. tostring(score), 10, 10)
end

function CheckCollision(x1, y1, w1, h1, x2, y2, w2, h2)
    return x1 < x2+w2 and
           x2 < x1+w1 and
           y1 < y2+h2 and
           y2 < y1+h1
end
